import { Hero } from './hero';

export const HEROES: Hero[] = [
  { id: 1, name: 'Vaai' },
  { id: 2, name: 'Ap' },
  { id: 3, name: 'Ak' },
  { id: 4, name: 'Imman' },
  { id: 5, name: 'Chidam' },
  { id: 6, name: 'Peepa' },
  { id: 7, name: 'Panda' },
  { id: 8, name: 'Daddy' },
  { id: 9, name: 'Mommy' },
  { id: 10, name: 'Chitty' }
];