import { Component } from '@angular/core';
import { User } from './user';
import { EnrollService } from './enroll.service';
import { error } from 'util';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  topics = ['angular', 'react', 'typescript'];
  userModel = new User('user', 'vino@g.com', 7894561233, 'default', 'morning', true);

  topicHasError = true;
  constructor(private _enrollService : EnrollService){}

  validatetopic(value) {
    if (value === 'default') {
      this.topicHasError = true;
    } else {
      this.topicHasError = false;
    }
  }
  onsubmit() {
    // console.log(this.userModel);
    this._enrollService.enroll(this.userModel)
    .subscribe(
      data => console.log('success!',data),
      error => console.error('error!',error)
    )
  }
}
